# Match It

A tool for running a catch matching game, based on a game that Lori and Corey Cole were using for giveaways during streams celebrating the 30th anniversary of Quest For Glory 1 (also known Hero's Quest).

1. Instructions
2. Licence



# 1. Instructions

## Installation

Clone the repository (or download via [this URL](https://gitlab.com/Cheeseness/match-it/-/archive/master/match-it-master.zip) and open index.html in a browser.


## General Instructions

1. Configure images by selecting "Set Up Images" from the main menu and adding/editing images as desired.
2. Configure any game settings as desired by selecting "Settings" from the main mneu. Individual settings are described below.
3. Select "Start Game" to begin the game with the first image.
4. Click on cards as players guess. Unmatched cards will be automatically unflipped unless they are a "joker."
5. The round is won when the "Reveal" button is clicked, or when the last card pair is matched.
6. Repeat until all images have been played.


## Setting Up Images

1. Select "Set Up Images" from the main menu to open.
2. Click the "Add" button to add a new image, or a image's "Edit" button to edit that image.
3. Select the desired image.
4. Optionally select an altered image, which will be used until all cards have been matched or the "Reveal" button has been pressed.
4. Optionally enter a different number of rows and columns (note that the total number of cards must not exceed the number of defined card fronts for this image - default is 12).
5. Optionally select a different card back.
5. Optionally select a different set of card fronts.
6. Optionally enter a description to be shown when the final image is revealed.


## Settings

### Background Colour
Clicking apply will set the background colour to the hex colour value specified in the corresponding text field. Invalid values will not be applied.

### Grid Colour
Clicking apply will set the grid colour to the hex colour value specified in the corresponding text field. Invalid values will not be applied.

### Draw Card Coords
When "Draw Card Coords" is set to true, grid coordinates will be drawn on card backs.

### Draw Grid
When "Draw Grid" is set to true, a spacer grid will be drawn between cards.

### Draw Grid Coords
When "Draw Grid Coords" is set to true, grid coordinates will be drawn along the top and left sides of the play space.

### Use Jokers
When "Use Jokers" is set to true, joker cards will be used (if defined in the image's selected card fronts), which will stay flipped and can be considered a "lost" turn.

### Flip Delay
Flip Delay controls the amount of time that cards will stay flipped before automatically unflipping or disappearing. Values are in milliseconds (1000 is one second).

### Reveal Delay
Reveal Delay controls the amount of time that the play space will remain visible before removing grid lines and coordinates, and showing the unaltered image and caption (depending on which of these things are enabled/configured). Values are in milliseconds (1000 is one second).




# 2. Licence

Match It is made available under the Creative Commons CC0 1.0 Public Domain dedication
https://creativecommons.org/publicdomain/zero/1.0/
